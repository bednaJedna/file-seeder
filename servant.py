from pathlib import Path
from os import chdir, getcwd
from sys import platform
from argparse import ArgumentParser
from subprocess import run


# argument parser
parser = ArgumentParser()

ops = parser.add_mutually_exclusive_group(required=True)
ops.add_argument(
    '-c', '--create',
    action='store_true',
    help='Create [file] in dir tree.'
)
ops.add_argument(
    '-d', '--delete',
    action='store_true',
    help='Delete [file] in dir tree.'
)
parser.add_argument(
    '-f', '--filename',
    action='store',
    help='Enter [filename].[ext] to create or delete.'
)
parser.add_argument(
    '-p', '--path',
    action='store',
    default="",
    help='Path to the root dir'
)
parser.add_argument(
    '--do-not-ignore',
    action='store_true',
    default=False,
    help='Do not ignore ignored type of folder. Curently ignored: .git/*'
)

args = parser.parse_args()

# global vars
_ignored_dirs = dict(git='.git')
_path = args.path if args.path.endswith('/') else ''.join([args.path, '/'])
_subdirs = sorted(Path(''.join([_path, '.'])).glob('**'))
_curdir = getcwd()


# printing class
class Notification(object):

    def __init__(self, file, directory):
        self.file = file
        self.directory = directory

    def is_created(self):
        print('File "{file}" in directory "{directory}" was created.'.format(
            file=self.file,
            directory=self.directory
        ))

    def is_deleted(self):
        print('File "{file}" in directory "{directory}" was deleted.'.format(
            file=self.file,
            directory=self.directory
        ))

    def is_ignored(self):
        print('Directory "{directory}" is ignored. Skipping...'.format(
            directory=self.directory
        ))


# func to create given file
def maker(filename: str, path_: str):

    for dir_ in _subdirs:
        try:
            chdir(dir_)

            for ignored in list(_ignored_dirs.values()):
                if (ignored in getcwd()) and (args.do_not_ignore is False):

                    Notification(None, dir_).is_ignored()
                    chdir(_curdir)
                    continue

                else:

                    _f = open(filename, mode='x')

                    Notification(_f.name, dir_).is_created()

                    _f.close()
                    chdir(_curdir)

        except FileExistsError as e:

            print('Error: "{}"'.format(e))
            chdir(_curdir)
            continue


# func to delete given file
def unmaker(filename: str, path_: str):

    for dir_ in _subdirs:
        try:

            chdir(dir_)
            _f = Path(filename)
            _f.unlink()

            Notification(_f.name, dir_).is_deleted()

            chdir(_curdir)

        except FileNotFoundError as e:

            print('Error: "{}"'.format(e))
            chdir(_curdir)
            continue


# func to show help
def show_help():

    if platform.startswith('linux'):
        run(
            ['python3', 'servant.py', '-h']
        )

    elif platform.startswith('win32'):
        run(
            ['python', 'servant.py', '-h']
        )

    else:
        raise OSError('Script supports Linux or Win environment.')


# main
def main():

    if args.filename is not None:
        if args.create:

            maker(args.filename, _path)

        elif args.delete:

            unmaker(args.filename, _path)

        else:
            show_help()

    else:
        show_help()


if __name__ == "__main__":

    main()
